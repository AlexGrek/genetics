open System.IO


type Node = {index: int; color: int; neighbours: int list}

let random = System.Random()
let mutable totalColors = 17
let mutable visitedNodes: int list = []
let mutable nodes: Node array = [||]
let mutable ants = [| 0; 0; 0; 0; 0 |]

let addlink x y =
    let ix = (int x) - 1
    let iy = (int y) - 1
    nodes.[ix] <- {index = ix; color = nodes.[ix].color; neighbours = iy :: nodes.[ix].neighbours}
    nodes.[iy] <- {index = iy; color = nodes.[iy].color; neighbours = ix :: nodes.[iy].neighbours}
    "Linked " + x + " to " + y


let initcolor n = 
    random.Next(0, totalColors)

let gennodes nodescount links =
    nodes <- Array.init (int nodescount) (fun i -> {index = i; color = (initcolor <| int nodescount); neighbours = []})
    "Nodes: " + nodescount + " links: " + links


let comment tail =
    "Comment: " + List.reduce (fun acc el -> acc + " " + el) tail


let split (str: string) =
    Array.toList <| str.Split([|' '|])

let parse str =
    let elems = split str
    match elems with
    | [ "e"; x; y ]                 -> addlink x y
    | [ "p"; "edge"; nodes; links ] -> gennodes nodes links
    | head :: tail when head = "c"  -> comment elems
    | _ -> failwith "Unparsed string"

let readLines (filePath:string) = seq {
    use sr = new StreamReader (filePath)
    while not sr.EndOfStream do
        yield sr.ReadLine ()
}

let printCall func el =
    //printfn " %A " el
    func el

let parsef file =
    Seq.map (printCall parse) <| readLines file
    |> Seq.toList


let isItBad index =
    let node = nodes.[index]
    let color = node.color
    List.filter (fun i -> nodes.[i].color = color) node.neighbours
    |> List.length


let isItBadNode node =
    let color = node.color
    List.filter (fun i -> nodes.[i].color = color) node.neighbours
    |> List.length

let printNode node =
    let visited = Seq.filter (fun x -> x = node.index) visitedNodes |> Seq.length
    printf "< %d > %d (%d) {%A} --- %d\n" node.index node.color visited node.neighbours (isItBadNode node)

let printNodes nodes =
    printf "Nodes: %d\n" (Array.length nodes)
    Array.iter printNode nodes


let calculatePn nodes iter =
    let conflicts    = Array.map isItBadNode nodes
    let maxconf      = Array.max conflicts |> float
    let confsoverall = Array.sum conflicts |> float
    let PROBLEM_SIZ  = Array.length nodes  |> float
    let avgy = 4.8* confsoverall / (PROBLEM_SIZ * PROBLEM_SIZ)
    let avgx = 10.0 * maxconf
    // printf "  > maxconf: %f, confsoverall: %f, PROBLEM_SIZ: %f, avgy: %f, avgx: %f\n" maxconf confsoverall PROBLEM_SIZ avgy avgx
    // printf "  | (5.0 * (float iter) + 1.0) = %f\n" (5.0 * (float iter) + 1.0)
    System.Math.Exp(-3.2 * ((5.0 * (float iter) + 1.0) * avgy / (avgx)))


let isGoodColor k node =
    let confl = isItBadNode node
    let possibleColors = List.init k id
    let analysed = List.map (fun x -> isItBadNode {index = node.index; color = x; neighbours = node.neighbours}, x) possibleColors
    printfn "Conflicts/colors for node %d: %A" node.index analysed
    List.map fst analysed |> List.min < confl


let selectBestColor k node =
    let possibleColors = List.init k id
    let analysed = List.map (fun x -> isItBadNode {index = node.index; color = x; neighbours = node.neighbours}, x) possibleColors
    let result = analysed |> List.minBy fst |> snd
    printfn "best color: %d" result
    result


let selectBestOfWorstColor k node =
    let possibleColors = Seq.init k id |> Seq.filter (fun x -> x <> node.color)
    let analysed = Seq.map (fun x -> isItBadNode {index = node.index; color = x; neighbours = node.neighbours}, x) possibleColors
    let sorted = Seq.sortBy fst analysed |> Seq.filter (fun x -> fst x <> 0) |> Seq.toList
    if not (List.isEmpty sorted) then
        printfn "Best of worst color (except 0) is (confl, color) %A" sorted.[0]
        snd sorted.[0]
    else
        let sorted2 = Seq.sortBy fst analysed |> Seq.toList
        printfn "Best of worst color (UNFILTERED) is (confl, color) %A" sorted2.[0]
        snd sorted2.[0]
    

let paint color inode =
    let node = nodes.[inode]
    printfn "! node %d:%d repainted to %d\n" inode node.color color
    nodes.[inode] <- { index = inode; color = color; neighbours = node.neighbours }
    visitedNodes  <- inode :: visitedNodes
    inode


let moveRandom inode = 
    let node = nodes.[inode]
    let nb = node.neighbours
    let lessvisited = List.map (fun n -> (List.filter (fun x -> x = n) visitedNodes |> List.length, n)) nb |> List.sortBy fst
    printfn "Visited nodes: %A, Less visited is %d" lessvisited (snd lessvisited.[0])
    (snd lessvisited.[0])
    


let decide pn = 
    random.NextDouble() > pn


let moveAnt nodes iter ant antPlace =
    let pn = calculatePn nodes iter
    let possibilities = List.map (fun x -> (isItBad x, x)) nodes.[antPlace].neighbours |> List.sortByDescending fst
    printf "\nAnt %d; iter %d; antPlace %d; pn = %f\n" ant iter antPlace pn
    if not (decide pn) then
        let worstnode = nodes.[snd possibilities.[0]]
        printfn "move to worst node(%d)" worstnode.index
        printfn "change color to best if exists"
        if (isGoodColor totalColors worstnode) then
            printfn "paint best color"
            paint (selectBestColor totalColors worstnode) worstnode.index
        else
            printfn "paint best of the worst"
            paint (selectBestOfWorstColor totalColors worstnode) worstnode.index
    else
        let randomnode = nodes.[moveRandom antPlace]
        printfn "move to random node (%d) with %d conflicts" randomnode.index (isItBadNode randomnode)
        printfn "change color if lucky"
        if (decide 0.5) then
            printfn "lucky, now try to paint best color"
            if (isGoodColor totalColors randomnode) then
                printfn "paint best color"
                paint (selectBestColor totalColors randomnode) randomnode.index
            else
                printfn "paint best of the worst"
                paint (selectBestOfWorstColor totalColors randomnode) randomnode.index
        else
            let randomnode = nodes.[snd possibilities.[random.Next(List.length possibilities)]]
            printfn "do nothing, just move to RANDOM %d node" randomnode.index
            visitedNodes <- randomnode.index :: visitedNodes
            randomnode.index


let rec iterate nodes iter =
    if iter = 2000 then
        printNodes nodes
        failwith ("Solution was not found in 2000 iterations, still " + string (Array.sumBy isItBadNode nodes) + " conflicts remaining") 
    match (Array.map isItBadNode nodes |> Array.max) with
    | 0 -> iter
    | _ -> 
        ants <- Array.mapi (fun i x -> moveAnt nodes iter i x) ants
        iterate nodes (iter + 1)
    
let main args =
    printf "Parsing..."
    parsef "yuzGCPrnd127.13.col" |> ignore
    printf "Done."
    Array.iter (printf " %A " << isItBadNode) nodes
    printNodes nodes
    printfn "Iterations: %A" <| iterate nodes 0
    printNodes nodes

main "";;