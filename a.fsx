let random = System.Random()

let randomize hb lb =
    (random.NextDouble() * (hb - lb)) + lb

let init lbound hbound count =
    List.init count (fun _ -> randomize hbound lbound)

let initall lbound hbound icount count =
    List.init count (fun _ -> init lbound hbound icount)


let solve1 func (el: float list) =
    (el, func(el.[0]))

let solve2 func (el: float list) =
    (el, func(el.[0], el.[1]))

let solve3 func (el: float list) =
    (el, func(el.[0], el.[1], el.[2]))

let solveall func ants =
    List.map (solve2 func) ants



let choose ants func count =
    let sorted = List.sortBy (fun a -> (snd a)) <| solveall func ants
    List.init count (fun i -> sorted.[i])


let myfunc (x1, x2) =
    x1 * x2


let mainfn =
    let initants = initall -5.0 5.0 2 5
    choose initants myfunc 2
